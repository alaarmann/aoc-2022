use std::{
    cmp::Reverse,
    collections::LinkedList,
    collections::{HashMap, HashSet},
    iter::once,
    sync::Arc,
};

fn main() {
    println!("Hello, world!");
}

fn _day_01_pt1(input: &str) -> u64 {
    input
        .split("\n\n")
        .map(|s| {
            s.split('\n')
                .filter(|s| !s.is_empty())
                .map(|n| n.parse::<u64>().unwrap())
                .sum()
        })
        .map(|s| dbg!(s))
        .max()
        .unwrap()
}

fn _day_01_pt2(input: &str) -> u64 {
    let mut calories_per_person: Vec<u64> = input
        .split("\n\n")
        .map(|s| {
            s.split('\n')
                .filter(|s| !s.is_empty())
                .map(|n| n.parse::<u64>().unwrap())
                .sum()
        })
        .collect();
    calories_per_person.sort_by_key(|w| Reverse(*w));
    calories_per_person.into_iter().take(3).sum()
}

fn _day_02_pt1(input: &str) -> u64 {
    input
        .split('\n')
        .filter(|s| !s.is_empty())
        .map(|s| {
            let shape_player1 = s.split(' ').next().map(_shape_from_str).unwrap().unwrap();

            let shape_player2 = s.split(' ').last().map(_shape_from_str).unwrap().unwrap();
            (shape_player1, shape_player2)
        })
        .map(|s| dbg!(s))
        .map(_calculate_points)
        .sum()
}

fn _day_02_pt2(input: &str) -> u64 {
    input
        .split('\n')
        .filter(|s| !s.is_empty())
        .map(|s| {
            let shape_player1 = s.split(' ').next().map(_shape_from_str).unwrap().unwrap();

            let outcome = s.split(' ').last().map(_outcome_from_str).unwrap().unwrap();
            (shape_player1, outcome)
        })
        .map(|s| dbg!(s))
        .map(_calculate_points_pt2)
        .sum()
}

fn _calculate_points((s1, s2): (Shape, Shape)) -> u64 {
    let value_of_shape = match s2 {
        Shape::Rock => 1,
        Shape::Paper => 2,
        Shape::Scissors => 3,
    };
    let value_of_outcome = _calculate_outcome((s1, s2));
    dbg!(value_of_outcome) + dbg!(value_of_shape)
}

fn _calculate_points_pt2((s1, o): (Shape, Outcome)) -> u64 {
    let value_of_outcome = _calculate_outcome_pt2(o.clone());
    let s2 = _calculate_shape2((s1, o));
    let value_of_shape = match s2 {
        Shape::Rock => 1,
        Shape::Paper => 2,
        Shape::Scissors => 3,
    };
    dbg!(value_of_outcome) + dbg!(value_of_shape)
}

fn _calculate_shape2((s1, o): (Shape, Outcome)) -> Shape {
    match (s1, o) {
        (Shape::Rock, Outcome::Lose) => Shape::Scissors,
        (Shape::Rock, Outcome::Draw) => Shape::Rock,
        (Shape::Rock, Outcome::Win) => Shape::Paper,
        (Shape::Paper, Outcome::Lose) => Shape::Rock,
        (Shape::Paper, Outcome::Draw) => Shape::Paper,
        (Shape::Paper, Outcome::Win) => Shape::Scissors,
        (Shape::Scissors, Outcome::Lose) => Shape::Paper,
        (Shape::Scissors, Outcome::Draw) => Shape::Scissors,
        (Shape::Scissors, Outcome::Win) => Shape::Rock,
    }
}

fn _calculate_outcome(p: (Shape, Shape)) -> u64 {
    match p {
        (Shape::Rock, Shape::Scissors) => 0,
        (Shape::Scissors, Shape::Paper) => 0,
        (Shape::Paper, Shape::Rock) => 0,
        (Shape::Rock, Shape::Rock) => 3,
        (Shape::Scissors, Shape::Scissors) => 3,
        (Shape::Paper, Shape::Paper) => 3,
        _ => 6,
    }
}

fn _calculate_outcome_pt2(o: Outcome) -> u64 {
    match o {
        Outcome::Lose => 0,
        Outcome::Draw => 3,
        Outcome::Win => 6,
    }
}

fn _shape_from_str(c: &str) -> Option<Shape> {
    match c {
        "A" => Some(Shape::Rock),
        "X" => Some(Shape::Rock),
        "B" => Some(Shape::Paper),
        "Y" => Some(Shape::Paper),
        "C" => Some(Shape::Scissors),
        "Z" => Some(Shape::Scissors),
        _ => None,
    }
}

fn _outcome_from_str(c: &str) -> Option<Outcome> {
    match c {
        "X" => Some(Outcome::Lose),
        "Y" => Some(Outcome::Draw),
        "Z" => Some(Outcome::Win),
        _ => None,
    }
}

#[derive(Debug)]
pub enum Shape {
    Rock,
    Paper,
    Scissors,
}

#[derive(Debug, Clone)]
pub enum Outcome {
    Lose,
    Draw,
    Win,
}

fn _day_03_pt1(input: &str) -> u64 {
    input
        .split('\n')
        .filter(|s| !s.is_empty())
        .map(|s| s.split_at(s.len() / 2))
        .map(_find_common)
        .map(_calculate_priority)
        .sum()
}

fn _day_03_pt2(input: &str) -> u64 {
    let (groups, _) = input.split('\n').filter(|s| !s.is_empty()).fold(
        (Vec::new(), 0),
        |(mut acc, index), item| {
            let mut current_group = if index == 0 {
                Vec::new()
            } else {
                acc.pop().unwrap()
            };
            current_group.push(item);
            acc.push(current_group);
            (acc, if index == 2 { 0 } else { index + 1 })
        },
    );

    groups
        .into_iter()
        .map(_find_common_in_group)
        .map(_calculate_priority)
        .sum()
}

fn _find_common_in_group(group: Vec<&str>) -> char {
    let first = group.clone().into_iter().next().unwrap();
    let second = group.clone().into_iter().nth(1).unwrap();
    let third = group.into_iter().nth(2).unwrap();
    first
        .split("")
        .filter(|s| !s.is_empty())
        .find(|c| second.contains(c) && third.contains(c))
        .map(|s| s.chars().next().unwrap())
        .unwrap()
}

fn _find_common((first, second): (&str, &str)) -> char {
    first
        .split("")
        .filter(|s| !s.is_empty())
        .find(|c| second.contains(c))
        .map(|s| s.chars().next().unwrap())
        .unwrap()
}

fn _calculate_priority(c: char) -> u64 {
    let num: u64 = c as u64;
    if (65..=90).contains(&num) {
        num - 38
    } else {
        num - 96
    }
}

fn _day_04_pt1(input: &str) -> u64 {
    input
        .split('\n')
        .filter(|s| !s.is_empty())
        .map(|s| {
            let (first, second) = _split_in_two_by(s, ',');

            (_split_in_two_by(first, '-'), _split_in_two_by(second, '-'))
        })
        .map(|((l1, u1), (l2, u2))| ((_to_u64(l1), _to_u64(u1)), (_to_u64(l2), _to_u64(u2))))
        .filter(_is_contained)
        .count()
        .try_into()
        .unwrap()
}

fn _is_contained(&((l1, u1), (l2, u2)): &((u64, u64), (u64, u64))) -> bool {
    ((l1..=u1).contains(&l2) && (l1..=u1).contains(&u2))
        || ((l2..=u2).contains(&l1) && (l2..=u2).contains(&u1))
}

fn _day_04_pt2(input: &str) -> u64 {
    input
        .split('\n')
        .filter(|s| !s.is_empty())
        .map(|s| {
            let (first, second) = _split_in_two_by(s, ',');

            (_split_in_two_by(first, '-'), _split_in_two_by(second, '-'))
        })
        .map(|((l1, u1), (l2, u2))| ((_to_u64(l1), _to_u64(u1)), (_to_u64(l2), _to_u64(u2))))
        .filter(_is_contained_at_all)
        .count()
        .try_into()
        .unwrap()
}

fn _is_contained_at_all(&((l1, u1), (l2, u2)): &((u64, u64), (u64, u64))) -> bool {
    ((l1..=u1).contains(&l2) || (l1..=u1).contains(&u2))
        || ((l2..=u2).contains(&l1) || (l2..=u2).contains(&u1))
}

fn _split_in_two_by(value: &str, separator: char) -> (&str, &str) {
    (
        value.split(separator).next().unwrap(),
        value.split(separator).last().unwrap(),
    )
}

fn _split_in_two_by_str<'a>(value: &'a str, separator: &'a str) -> (&'a str, &'a str) {
    (
        value.split(separator).next().unwrap(),
        value.split(separator).last().unwrap(),
    )
}

fn _to_u64(value: &str) -> u64 {
    value.parse::<u64>().unwrap()
}

fn _day_05_pt1(input: &str) -> String {
    let (stacks, moves) = _split_in_two_by_str(input, "\n\n");
    let stacks: Vec<LinkedList<char>> = _parse_stacks(stacks);
    let moves: Vec<(usize, usize, usize)> = _parse_moves(moves);

    let resulting_stacks = moves
        .into_iter()
        .fold(stacks, |mut acc, (count, from, to)| {
            (0..count).into_iter().for_each(|_| {
                let item = acc.get_mut(from - 1).and_then(|l| l.pop_front());
                if let Some(v) = item {
                    if let Some(l) = acc.get_mut(to - 1) {
                        l.push_front(v)
                    }
                };
            });
            acc
        });

    resulting_stacks
        .into_iter()
        .map(|mut l| l.pop_front().unwrap())
        .collect()
}

fn _day_05_pt2(input: &str) -> String {
    let (stacks, moves) = _split_in_two_by_str(input, "\n\n");
    let stacks: Vec<LinkedList<char>> = _parse_stacks(stacks);
    let moves: Vec<(usize, usize, usize)> = _parse_moves(moves);

    let resulting_stacks = moves
        .into_iter()
        .fold(stacks, |mut acc, (count, from, to)| {
            let mut popped: Vec<char> = (0..count)
                .into_iter()
                .map(|_| acc.get_mut(from - 1).and_then(|l| l.pop_front()).unwrap())
                .collect();
            popped.reverse();
            popped.into_iter().for_each(|c| {
                if let Some(l) = acc.get_mut(to - 1) {
                    l.push_front(c)
                }
            });
            acc
        });

    resulting_stacks
        .into_iter()
        .map(|mut l| l.pop_front().unwrap())
        .collect()
}

fn _parse_moves(moves: &str) -> Vec<(usize, usize, usize)> {
    moves
        .split('\n')
        .filter(|s| !s.is_empty())
        .map(|s| {
            let tokens: Vec<&str> = s.split(' ').collect();
            (
                _to_usize(tokens.get(1).unwrap()),
                _to_usize(tokens.get(3).unwrap()),
                _to_usize(tokens.get(5).unwrap()),
            )
        })
        .collect()
}

fn _to_usize(value: &str) -> usize {
    value.parse::<usize>().unwrap()
}

fn _parse_stacks(stacks: &str) -> Vec<LinkedList<char>> {
    stacks
        .split('\n')
        .filter(|s| !s.is_empty())
        .filter(|s| !s.starts_with(" 1"))
        .map(_parse_stack_line)
        .fold(Vec::new(), |mut acc, line_items| {
            if acc.is_empty() {
                line_items.iter().for_each(|_| acc.push(LinkedList::new()));
            }
            line_items
                .into_iter()
                .enumerate()
                .for_each(|(index, item)| {
                    if let Some(value) = item {
                        if let Some(stack) = acc.get_mut(index) {
                            stack.push_back(value);
                        }
                    }
                });
            acc
        })
}

fn _parse_stack_line(line: &str) -> Vec<Option<char>> {
    dbg!(line);
    line.char_indices()
        .filter(|&(i, _)| (i != 0) && (((i - 1) % 4) == 0))
        .map(|(_, c)| match c {
            ' ' => None,
            _ => Some(c),
        })
        .collect()
}

fn _extract_item(value: &str) -> Option<char> {
    Some(value)
        .and_then(|s| s.chars().nth(1))
        .filter(|&c| c != ' ')
}

fn _day_06(input: &str, buf_len: usize) -> usize {
    let (position, _) = input
        .char_indices()
        .fold((0, String::new()), |acc, (p, c)| {
            if acc.1.len() == buf_len && _all_different(&acc.1) {
                acc
            } else {
                let mut buf = acc.1;
                buf.push(c);
                let resized_buffer = if buf.len() > buf_len {
                    buf.split_off(buf.len() - buf_len)
                } else {
                    buf
                };
                (p, resized_buffer)
            }
        });
    position + 1
}

fn _all_different(buf: &str) -> bool {
    let counts = dbg!(buf)
        .chars()
        .fold(HashMap::new(), |mut acc: HashMap<char, usize>, c| {
            let existing = acc.get(&c);
            let next_count = if let Some(v) = existing { v + 1 } else { 1 };
            acc.insert(c, next_count);
            acc
        });
    counts
        .into_iter()
        .map(|(_, cnt)| cnt)
        .filter(|&cnt| cnt > 1)
        .count()
        == 0
}

fn _day_07_pt1(input: &str) -> u64 {
    let (_, dir_list): (Option<String>, HashMap<String, Vec<Node<u64>>>) = input
        .split("$ ")
        .filter(|s| !s.is_empty())
        .fold((None, HashMap::new()), |(cur_dir, mut dirs), cmd_str| {
            let cmd_components: Vec<&str> = cmd_str.split('\n').filter(|s| !s.is_empty()).collect();
            let cmd_components = dbg!(cmd_components);
            let cmd: &str = cmd_components.get(0).unwrap().split(' ').next().unwrap();
            match cmd {
                "cd" => {
                    let cmd_arg: &str = cmd_components.get(0).unwrap().split(' ').nth(1).unwrap();
                    match cmd_arg {
                        "/" => (Some("/".to_string()), dirs),
                        ".." => {
                            let cur_dir = dbg!(cur_dir.unwrap());
                            let levels: Vec<&str> = dbg!(cur_dir.split('/').collect());
                            let next_cnt_levels = levels.len() - 1;
                            let parent_dir = dbg!(if next_cnt_levels > 1 {
                                levels[0..next_cnt_levels].join("/")
                            } else {
                                "/".to_string()
                            });
                            (Some(parent_dir), dirs)
                        }
                        _ => {
                            let next_dir = format!(
                                "{}/{}",
                                cur_dir
                                    .filter(|s| s != "/")
                                    .unwrap_or_else(|| "".to_string()),
                                cmd_arg
                            );
                            (Some(next_dir), dirs)
                        }
                    }
                }

                "ls" => {
                    let content = cmd_components
                        .into_iter()
                        .skip(1)
                        .map(|s| (s.split(' ').next().unwrap(), s.split(' ').nth(1).unwrap()))
                        .map(|(data, name)| {
                            if data.starts_with("dir") {
                                Node::Dir(name.to_string())
                            } else {
                                Node::File(name.to_string(), _to_u64(data))
                            }
                        })
                        .collect();

                    dirs.insert(cur_dir.clone().unwrap(), content);
                    (cur_dir, dirs)
                }
                _ => (cur_dir, dirs),
            }
        });
    let dir_list = dbg!(dir_list);
    let netto_dir_sizes: Vec<(String, u64)> = dir_list
        .iter()
        .map(|(dir, cont)| {
            (
                dir.clone(),
                cont.iter()
                    .map(|dir_ent| match dir_ent {
                        Node::File(_, siz) => *siz,
                        _ => 0,
                    })
                    .sum::<u64>(),
            )
        })
        .collect();
    let brutto_dir_sizes: Vec<(String, u64)> = netto_dir_sizes
        .clone()
        .into_iter()
        .map(|(dir, size)| {
            let contained_dir_size_sum: u64 = netto_dir_sizes
                .iter()
                .filter(|(net_dir, _)| net_dir != &dir && net_dir.starts_with(&dir))
                .map(|(_, net_size)| net_size)
                .sum();
            (dir, size + contained_dir_size_sum)
        })
        .collect();

    let brutto_dir_sizes = dbg!(brutto_dir_sizes);
    brutto_dir_sizes
        .into_iter()
        .filter(|(_dir, size)| *size <= 100000)
        .map(|(_, size)| size)
        .sum()
}

fn _day_07_pt2(input: &str) -> u64 {
    let (_, dir_list): (Option<String>, HashMap<String, Vec<Node<u64>>>) = input
        .split("$ ")
        .filter(|s| !s.is_empty())
        .fold((None, HashMap::new()), |(cur_dir, mut dirs), cmd_str| {
            let cmd_components: Vec<&str> = cmd_str.split('\n').filter(|s| !s.is_empty()).collect();
            let cmd_components = dbg!(cmd_components);
            let cmd: &str = cmd_components.get(0).unwrap().split(' ').next().unwrap();
            match cmd {
                "cd" => {
                    let cmd_arg: &str = cmd_components.get(0).unwrap().split(' ').nth(1).unwrap();
                    match cmd_arg {
                        "/" => (Some("/".to_string()), dirs),
                        ".." => {
                            let cur_dir = dbg!(cur_dir.unwrap());
                            let levels: Vec<&str> = dbg!(cur_dir.split('/').collect());
                            let next_cnt_levels = levels.len() - 1;
                            let parent_dir = dbg!(if next_cnt_levels > 1 {
                                levels[0..next_cnt_levels].join("/")
                            } else {
                                "/".to_string()
                            });
                            (Some(parent_dir), dirs)
                        }
                        _ => {
                            let next_dir = format!(
                                "{}/{}",
                                cur_dir
                                    .filter(|s| s != "/")
                                    .unwrap_or_else(|| "".to_string()),
                                cmd_arg
                            );
                            (Some(next_dir), dirs)
                        }
                    }
                }

                "ls" => {
                    let content = cmd_components
                        .into_iter()
                        .skip(1)
                        .map(|s| (s.split(' ').next().unwrap(), s.split(' ').nth(1).unwrap()))
                        .map(|(data, name)| {
                            if data.starts_with("dir") {
                                Node::Dir(name.to_string())
                            } else {
                                Node::File(name.to_string(), _to_u64(data))
                            }
                        })
                        .collect();

                    dirs.insert(cur_dir.clone().unwrap(), content);
                    (cur_dir, dirs)
                }
                _ => (cur_dir, dirs),
            }
        });
    let dir_list = dbg!(dir_list);
    let netto_dir_sizes: Vec<(String, u64)> = dir_list
        .iter()
        .map(|(dir, cont)| {
            (
                dir.clone(),
                cont.iter()
                    .map(|dir_ent| match dir_ent {
                        Node::File(_, siz) => *siz,
                        _ => 0,
                    })
                    .sum::<u64>(),
            )
        })
        .collect();
    let brutto_dir_sizes: Vec<(String, u64)> = netto_dir_sizes
        .clone()
        .into_iter()
        .map(|(dir, size)| {
            let contained_dir_size_sum: u64 = netto_dir_sizes
                .iter()
                .filter(|(net_dir, _)| net_dir != &dir && net_dir.starts_with(&dir))
                .map(|(_, net_size)| net_size)
                .sum();
            (dir, size + contained_dir_size_sum)
        })
        .collect();

    let mut brutto_dir_sizes = dbg!(brutto_dir_sizes);
    let space_used: u64 = brutto_dir_sizes
        .clone()
        .into_iter()
        .filter(|(dir, _)| dir == "/")
        .map(|(_, size)| size)
        .next()
        .unwrap();

    let space_needed = 30000000 - (70000000 - space_used);
    brutto_dir_sizes.sort_by(|(_, s1), (_, s2)| s1.cmp(s2));
    let brutto_dir_sizes = dbg!(brutto_dir_sizes);
    brutto_dir_sizes
        .into_iter()
        .filter(|(_, size)| {
            dbg!(*size, space_needed);
            *size >= space_needed
        })
        .map(|(_, size)| size)
        .next()
        .unwrap()
}

#[derive(Debug)]
pub enum Node<T> {
    Dir(String),
    File(String, T),
}

fn _day_08_pt1(input: &str) -> usize {
    let mat: Vec<Vec<u8>> = input
        .split('\n')
        .filter(|s| !s.is_empty())
        .map(|s| s.chars().map(|c| c as u8).map(|n| n - 48).collect())
        .collect();
    let max_x = mat.get(0).map(|l| l.len()).unwrap();
    let max_y = mat.len();
    let visible_from_left = _extract_visible(mat.clone());
    let visible_from_top =
        _turn_to_left_from_top(_extract_visible(_turn_to_top_from_left(mat.clone())), max_x);
    let visible_from_right = _turn_to_left_from_right(
        _extract_visible(_turn_to_right_from_left(mat.clone())),
        max_x,
        max_y,
    );
    let visible_from_bottom =
        _turn_to_left_from_bottom(_extract_visible(_turn_to_bottom_from_left(mat)), max_y);

    visible_from_left
        .into_iter()
        .chain(visible_from_top.into_iter())
        .chain(visible_from_right.into_iter())
        .chain(visible_from_bottom.into_iter())
        .collect::<HashSet<(usize, usize)>>()
        .len()
}

fn _turn_to_left_from_top(positions: Vec<(usize, usize)>, max_x: usize) -> Vec<(usize, usize)> {
    positions
        .iter()
        .map(|(x, y)| (*y, (max_x - 1) - *x))
        .collect()
}

fn _turn_to_left_from_right(
    positions: Vec<(usize, usize)>,
    max_x: usize,
    max_y: usize,
) -> Vec<(usize, usize)> {
    positions
        .iter()
        .map(|(x, y)| ((max_x - 1) - *x, (max_y - 1) - *y))
        .collect()
}

fn _turn_to_left_from_bottom(positions: Vec<(usize, usize)>, max_y: usize) -> Vec<(usize, usize)> {
    positions
        .iter()
        .map(|(x, y)| ((max_y - 1) - *y, *x))
        .collect()
}

fn _turn_to_top_from_left(mat: Vec<Vec<u8>>) -> Vec<Vec<u8>> {
    let mut result: Vec<Vec<u8>> = mat.iter().fold(Vec::new(), |mut acc, line| {
        line.iter().enumerate().for_each(|(x, value)| {
            if let Some(cols) = acc.get_mut(x) {
                cols.push(*value);
            } else {
                let new_cols = vec![*value];
                acc.push(new_cols);
            }
        });
        acc
    });
    result.reverse();
    result
}

fn _turn_to_right_from_left(mat: Vec<Vec<u8>>) -> Vec<Vec<u8>> {
    let mut result: Vec<Vec<u8>> = mat
        .into_iter()
        .map(|mut v| {
            v.reverse();
            v
        })
        .collect();
    result.reverse();
    result
}

fn _turn_to_bottom_from_left(mat: Vec<Vec<u8>>) -> Vec<Vec<u8>> {
    let result: Vec<Vec<u8>> = mat.iter().fold(Vec::new(), |mut acc, line| {
        line.iter().enumerate().for_each(|(x, value)| {
            if let Some(cols) = acc.get_mut(x) {
                cols.push(*value);
            } else {
                let new_cols = vec![*value];
                acc.push(new_cols);
            }
        });
        acc
    });
    let result: Vec<Vec<u8>> = result
        .into_iter()
        .map(|mut v| {
            v.reverse();
            v
        })
        .collect();
    result
}

fn _extract_visible(mat: Vec<Vec<u8>>) -> Vec<(usize, usize)> {
    mat.iter()
        .enumerate()
        .map(|(y, line)| {
            line.iter()
                .enumerate()
                .map(|(x, val)| (y, x, *val))
                .collect()
        })
        .flat_map(|line: Vec<(usize, usize, u8)>| {
            line.iter()
                .fold(
                    (None, Vec::new()),
                    |(max_height, mut visible): (_, Vec<(usize, usize)>), (x, y, val)| {
                        if let Some(cur_max_height) = max_height {
                            if *val > cur_max_height {
                                visible.push((*x, *y));
                                (Some(*val), visible)
                            } else {
                                (max_height, visible)
                            }
                        } else {
                            visible.push((*x, *y));
                            (Some(*val), visible)
                        }
                    },
                )
                .1
        })
        .collect()
}

fn _day_08_pt2(input: &str) -> usize {
    let mat: Vec<Vec<u8>> = input
        .split('\n')
        .filter(|s| !s.is_empty())
        .map(|s| s.chars().map(|c| c as u8).map(|n| n - 48).collect())
        .collect();
    let max_x = mat.get(0).map(|l| l.len()).unwrap();
    let max_y = mat.len();

    (0..max_y)
        .into_iter()
        .map(|y| {
            (0..max_x)
                .into_iter()
                .map(|x| {
                    let height = _extract_height(y, x, &mat);
                    let score_left = _calculate_score_left(y, x, height, &mat);
                    let score_top = _calculate_score_top(y, x, height, &mat);
                    let score_right = _calculate_score_right(y, x, height, max_x, &mat);
                    let score_bottom = _calculate_score_bottom(y, x, height, max_y, &mat);

                    dbg!(y, x, score_left, score_top, score_right, score_bottom);
                    score_left * score_top * score_right * score_bottom
                })
                .max()
                .unwrap()
        })
        .max()
        .unwrap()
}

fn _calculate_score_left(y: usize, x: usize, height: u8, mat: &Vec<Vec<u8>>) -> usize {
    (0..x)
        .rev()
        .map(|cur_x| _extract_height(y, cur_x, mat))
        .fold((0, false), |(cnt, stop), h| {
            if !stop {
                if h < height {
                    (cnt + 1, false)
                } else {
                    (cnt + 1, true)
                }
            } else {
                (cnt, stop)
            }
        })
        .0
}

fn _calculate_score_top(y: usize, x: usize, height: u8, mat: &Vec<Vec<u8>>) -> usize {
    (0..y)
        .rev()
        .map(|cur_y| _extract_height(cur_y, x, mat))
        .fold((0, false), |(cnt, stop), h| {
            if !stop {
                if h < height {
                    (cnt + 1, false)
                } else {
                    (cnt + 1, true)
                }
            } else {
                (cnt, stop)
            }
        })
        .0
}

fn _calculate_score_right(
    y: usize,
    x: usize,
    height: u8,
    max_x: usize,
    mat: &Vec<Vec<u8>>,
) -> usize {
    (x..max_x)
        .skip(1)
        .map(|cur_x| _extract_height(y, cur_x, mat))
        .fold((0, false), |(cnt, stop), h| {
            if !stop {
                if h < height {
                    (cnt + 1, false)
                } else {
                    (cnt + 1, true)
                }
            } else {
                (cnt, stop)
            }
        })
        .0
}

fn _calculate_score_bottom(
    y: usize,
    x: usize,
    height: u8,
    max_y: usize,
    mat: &Vec<Vec<u8>>,
) -> usize {
    (y..max_y)
        .skip(1)
        .map(|cur_y| _extract_height(cur_y, x, mat))
        .fold((0, false), |(cnt, stop), h| {
            if !stop {
                if h < height {
                    (cnt + 1, false)
                } else {
                    (cnt + 1, true)
                }
            } else {
                (cnt, stop)
            }
        })
        .0
}

fn _extract_height(y: usize, x: usize, mat: &Vec<Vec<u8>>) -> u8 {
    *(mat.get(y).unwrap().get(x).unwrap())
}

fn _day_09_pt1(input: &str) -> usize {
    input
        .split('\n')
        .filter(|s| !s.is_empty())
        .map(|s| _split_in_two_by(s, ' '))
        .flat_map(move |(direction, move_cnt)| (0.._to_usize(move_cnt)).map(move |_| direction))
        .fold(
            ((0, 0), (0, 0), vec![(0, 0)]),
            |((hx, hy), (tx, ty), mut tail_positions), direction| match direction {
                "R" => {
                    let new_head = (hx + 1, hy);
                    let new_tail = _calculate_tail(new_head, (tx, ty));
                    if new_tail != (tx, ty) {
                        tail_positions.push(new_tail);
                    };
                    (new_head, new_tail, tail_positions)
                }
                "L" => {
                    let new_head = (hx - 1, hy);
                    let new_tail = _calculate_tail(new_head, (tx, ty));
                    if new_tail != (tx, ty) {
                        tail_positions.push(new_tail);
                    };
                    (new_head, new_tail, tail_positions)
                }
                "U" => {
                    let new_head = (hx, hy + 1);
                    let new_tail = _calculate_tail(new_head, (tx, ty));
                    if new_tail != (tx, ty) {
                        tail_positions.push(new_tail);
                    };
                    (new_head, new_tail, tail_positions)
                }
                "D" => {
                    let new_head = (hx, hy - 1);
                    let new_tail = _calculate_tail(new_head, (tx, ty));
                    if new_tail != (tx, ty) {
                        tail_positions.push(new_tail);
                    };
                    (new_head, new_tail, tail_positions)
                }
                _ => ((hx, hy), (tx, ty), tail_positions),
            },
        )
        .2
        .into_iter()
        .collect::<HashSet<(i32, i32)>>()
        .len()
}

fn _day_09_pt2(input: &str) -> usize {
    input
        .split('\n')
        .filter(|s| !s.is_empty())
        .map(|s| _split_in_two_by(s, ' '))
        .flat_map(move |(direction, move_cnt)| (0.._to_usize(move_cnt)).map(move |_| direction))
        .fold(
            ((0, 0), [(0, 0); 9], vec![(0, 0)]),
            |((hx, hy), tail_knots, mut tail_positions), direction| match direction {
                "R" => {
                    let new_head = (hx + 1, hy);
                    let new_tail_knots = _calculate_tail_knots(new_head, tail_knots);
                    if new_tail_knots[8] != tail_knots[8] {
                        tail_positions.push(new_tail_knots[8]);
                    };
                    (new_head, new_tail_knots, tail_positions)
                }
                "L" => {
                    let new_head = (hx - 1, hy);
                    let new_tail_knots = _calculate_tail_knots(new_head, tail_knots);
                    if new_tail_knots[8] != tail_knots[8] {
                        tail_positions.push(new_tail_knots[8]);
                    };
                    (new_head, new_tail_knots, tail_positions)
                }
                "U" => {
                    let new_head = (hx, hy + 1);
                    let new_tail_knots = _calculate_tail_knots(new_head, tail_knots);
                    if new_tail_knots[8] != tail_knots[8] {
                        tail_positions.push(new_tail_knots[8]);
                    };
                    (new_head, new_tail_knots, tail_positions)
                }
                "D" => {
                    let new_head = (hx, hy - 1);
                    let new_tail_knots = _calculate_tail_knots(new_head, tail_knots);
                    if new_tail_knots[8] != tail_knots[8] {
                        tail_positions.push(new_tail_knots[8]);
                    };
                    (new_head, new_tail_knots, tail_positions)
                }
                _ => ((hx, hy), tail_knots, tail_positions),
            },
        )
        .2
        .into_iter()
        .collect::<HashSet<(i32, i32)>>()
        .len()
}

fn _calculate_tail_knots(head: (i32, i32), mut tail_knots: [(i32, i32); 9]) -> [(i32, i32); 9] {
    (0..9).for_each(|index| {
        tail_knots[index] = if index == 0 {
            _calculate_tail(head, tail_knots[index])
        } else {
            _calculate_tail(tail_knots[index - 1], tail_knots[index])
        }
    });
    tail_knots
}

fn _calculate_tail(head: (i32, i32), tail: (i32, i32)) -> (i32, i32) {
    if head.0 == tail.0 {
        let diff = head.1 - tail.1;
        if diff.abs() > 1 {
            (tail.0, diff.signum() + tail.1)
        } else {
            tail
        }
    } else if head.1 == tail.1 {
        let diff = head.0 - tail.0;
        if diff.abs() > 1 {
            (diff.signum() + tail.0, tail.1)
        } else {
            tail
        }
    } else {
        let diff_x = head.0 - tail.0;
        let diff_y = head.1 - tail.1;
        if diff_x.abs() > 1 || diff_y.abs() > 1 {
            (diff_x.signum() + tail.0, diff_y.signum() + tail.1)
        } else {
            tail
        }
    }
}

fn _day_10_pt1(input: &str) -> i64 {
    input
        .split('\n')
        .filter(|s| !s.is_empty())
        .flat_map(move |cmd| {
            if cmd.starts_with("addx") {
                vec!["addx 0", cmd].into_iter()
            } else {
                vec![cmd].into_iter()
            }
        })
        .fold((1, 1, 0), |(cycle_cnt, reg_x, mut sum), cmd| {
            dbg!(cmd);
            dbg!(reg_x);
            if [20, 60, 100, 140, 180, 220].contains(&cycle_cnt) {
                sum += dbg!(cycle_cnt) * dbg!(reg_x);
            }
            if cmd.starts_with("noop") {
                (cycle_cnt + 1, reg_x, sum)
            } else if cmd.starts_with("addx") {
                let (_, val) = _split_in_two_by(cmd, ' ');
                let val = _to_i64(val);

                (cycle_cnt + 1, reg_x + val, sum)
            } else {
                (cycle_cnt, reg_x, sum)
            }
        })
        .2
}

fn _day_10_pt2(input: &str) -> Vec<String> {
    input
        .split('\n')
        .filter(|s| !s.is_empty())
        .flat_map(move |cmd| {
            if cmd.starts_with("addx") {
                vec!["addx 0", cmd].into_iter()
            } else {
                vec![cmd].into_iter()
            }
        })
        .fold((1, 1, Vec::new()), |(cycle_cnt, reg_x, mut images), cmd| {
            dbg!(cmd);
            dbg!(reg_x);
            if cycle_cnt - 1 % 240 == 0 {
                images.push(String::new());
            }
            let images_len = images.len();
            let image = images.get_mut(images_len - 1).unwrap();
            let crt_pos = (image.len() % 40) as i64;
            let pixel = if (crt_pos - reg_x).abs() <= 1 {
                '#'
            } else {
                '.'
            };
            image.push(pixel);
            if cmd.starts_with("noop") {
                (cycle_cnt + 1, reg_x, images)
            } else if cmd.starts_with("addx") {
                let (_, val) = _split_in_two_by(cmd, ' ');
                let val = _to_i64(val);

                (cycle_cnt + 1, reg_x + val, images)
            } else {
                (cycle_cnt, reg_x, images)
            }
        })
        .2
        .into_iter()
        .map(|s| {
            s.chars()
                .enumerate()
                .flat_map(|(index, c)| {
                    if index != 0 && (index + 1) % 40 == 0 {
                        vec![c, '\n'].into_iter()
                    } else {
                        vec![c].into_iter()
                    }
                })
                .collect()
        })
        .collect()
}

fn _to_i64(value: &str) -> i64 {
    value.parse::<i64>().unwrap()
}

fn _day_11_pt1(input: &str) -> usize {
    let players = input
        .split("\n\n")
        .filter(|s| !s.is_empty())
        .map(Player::from)
        .collect();

    let mut players: Vec<Player> = (0..20).fold(players, |mut acc, round| {
        let number_of_players = acc.len();
        (0..number_of_players).for_each(|np| {
            let cloned_acc = acc.clone();
            let player = cloned_acc.get(dbg!(np)).unwrap();
            let operation = &player.operation;
            let find_player = &player.find_player;
            let number_of_items = player.items.len();
            player.items.iter().for_each(|item| {
                let level = (operation)(*item);
                let level = level / 3;
                let next_player = (find_player)(dbg!(level));
                acc.get_mut(next_player).unwrap().items.push(level);
            });
            acc.get_mut(np).unwrap().items.clear();
            let count_before = acc.get_mut(np).unwrap().count;
            acc.get_mut(np).unwrap().count = count_before + number_of_items;
        });
        dbg!(round);
        acc.iter().for_each(|p| {
            dbg!(&p.items);
            dbg!(&p.count);
        });
        acc
    });
    players.sort_by_key(|p| p.count);
    players.reverse();
    players.into_iter().map(|p| p.count).take(2).product()
}

#[derive(Clone)]
pub struct Player {
    count: usize,
    items: Vec<usize>,
    operation: Arc<dyn Fn(usize) -> usize>,
    find_player: Arc<dyn Fn(usize) -> usize>,
}

impl Player {
    pub fn from(input: &str) -> Player {
        let lines: Vec<&str> = input.split('\n').filter(|s| !s.is_empty()).collect();
        let lines = dbg!(lines);
        let items: Vec<usize> = lines
            .get(1)
            .map(|s| _split_in_two_by_str(s, ": ").1)
            .map(|s| s.split(", ").map(_to_usize).collect())
            .unwrap();
        let operation: Arc<dyn Fn(usize) -> usize> = lines
            .get(2)
            .map(|s| _split_in_two_by_str(s, ": new = ").1)
            .map(|s| {
                if s.starts_with("old *") {
                    match _split_in_two_by_str(s, "* ").1 {
                        "old" => Arc::new(|v| v * v),
                        operand => {
                            let operand = _to_usize(operand);
                            Arc::new(move |v| v * operand) as Arc<dyn Fn(usize) -> usize>
                        }
                    }
                } else {
                    let operand = _to_usize(_split_in_two_by_str(s, "+ ").1);
                    Arc::new(move |v| v + operand)
                }
            })
            .unwrap();

        let test_operand = lines
            .get(3)
            .map(|s| _split_in_two_by_str(s, "divisible by ").1)
            .map(_to_usize)
            .unwrap();

        let playerno_true = lines
            .get(4)
            .map(|s| _split_in_two_by_str(s, "true: throw to monkey ").1)
            .map(_to_usize)
            .unwrap();

        let playerno_false = lines
            .get(5)
            .map(|s| _split_in_two_by_str(s, "false: throw to monkey ").1)
            .map(_to_usize)
            .unwrap();

        let find_player: Arc<dyn Fn(usize) -> usize> = Arc::new(move |v| {
            if (v % test_operand) == 0usize {
                playerno_true
            } else {
                playerno_false
            }
        });

        Player {
            count: 0,
            items,
            operation,
            find_player,
        }
    }
}
#[cfg(test)]
mod tests {
    use std::fs;

    use super::*;

    #[test]
    fn test_day_01() {
        let test_input = "1000
2000
3000

4000

5000
6000

7000
8000
9000

10000";
        assert_eq!(_day_01_pt1(test_input), 24000);
        assert_eq!(_day_01_pt2(test_input), 45000);
    }

    #[test]
    fn day_01() {
        let test_input = fs::read_to_string("data/day_01/input").unwrap();
        assert_eq!(_day_01_pt1(&test_input), 72070);
        assert_eq!(_day_01_pt2(&test_input), 211805);
    }

    #[test]
    fn test_day_02() {
        let test_input = "A Y
B X
C Z";
        assert_eq!(_day_02_pt1(test_input), 15);
        assert_eq!(_day_02_pt2(test_input), 12);
    }

    #[test]
    fn day_02() {
        let test_input = fs::read_to_string("data/day_02/input").unwrap();
        assert_eq!(_day_02_pt1(&test_input), 13675);
        assert_eq!(_day_02_pt2(&test_input), 14184);
    }

    #[test]
    fn test_day_03() {
        let test_input = "vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw";
        assert_eq!(_day_03_pt1(test_input), 157);
        assert_eq!(_day_03_pt2(test_input), 70);
    }

    #[test]
    fn day_03() {
        let test_input = fs::read_to_string("data/day_03/input_day1.lst").unwrap();
        assert_eq!(_day_03_pt1(&test_input), 8153);
        assert_eq!(_day_03_pt2(&test_input), 2342);
    }

    #[test]
    fn test_day_04() {
        let test_input = "2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8";
        assert_eq!(_day_04_pt1(test_input), 2);
        assert_eq!(_day_04_pt2(test_input), 4);
    }

    #[test]
    fn day_04() {
        let test_input = fs::read_to_string("data/day_04/input").unwrap();
        assert_eq!(_day_04_pt1(&test_input), 657);
        assert_eq!(_day_04_pt2(&test_input), 938);
    }

    #[test]
    fn test_day_05() {
        let test_input = "    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2";
        assert_eq!(_day_05_pt1(test_input), "CMZ");
        assert_eq!(_day_05_pt2(test_input), "MCD");
    }

    #[test]
    fn day_05() {
        let test_input = fs::read_to_string("data/day_05/input").unwrap();
        assert_eq!(_day_05_pt1(&test_input), "JRVNHHCSJ");
        assert_eq!(_day_05_pt2(&test_input), "GNFBSBJLH");
    }

    #[test]
    fn test_day_06() {
        let test_input = "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw";
        assert_eq!(_day_06(test_input, 4), 11);
        assert_eq!(_day_06(test_input, 14), 26);
    }

    #[test]
    fn day_06() {
        let test_input = fs::read_to_string("data/day_06/input").unwrap();
        assert_eq!(_day_06(&test_input, 4), 1155);
        assert_eq!(_day_06(&test_input, 14), 2789);
    }

    #[test]
    fn test_day_07() {
        let test_input = "$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k";
        assert_eq!(_day_07_pt1(test_input), 95437);
        assert_eq!(_day_07_pt2(test_input), 24933642);
    }

    #[test]
    fn day_07() {
        let test_input = fs::read_to_string("data/day_07/input").unwrap();
        assert_eq!(_day_07_pt1(&test_input), 1555642);
        assert_eq!(_day_07_pt2(&test_input), 5974547);
    }

    #[test]
    fn test_day_08() {
        let test_input = "30373
25512
65332
33549
35390";
        assert_eq!(_day_08_pt1(test_input), 21);
        assert_eq!(_day_08_pt2(test_input), 8);
    }

    #[test]
    fn day_08() {
        let test_input = fs::read_to_string("data/day_08/input").unwrap();
        assert_eq!(_day_08_pt1(&test_input), 1693);
        assert_eq!(_day_08_pt2(&test_input), 422059);
    }

    #[test]
    fn test_day_09() {
        let test_input = "R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2";
        assert_eq!(_day_09_pt1(test_input), 13);
        let test_input = "R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20";
        assert_eq!(_day_09_pt2(test_input), 36);
    }

    #[test]
    fn day_09() {
        let test_input = fs::read_to_string("data/day_09/input").unwrap();
        assert_eq!(_day_09_pt1(&test_input), 6391);
        assert_eq!(_day_09_pt2(&test_input), 2593);
    }

    #[test]
    fn test_day_10() {
        let test_input = get_test_input_day_10();
        assert_eq!(_day_10_pt1(test_input), 13140);

        let expected = vec![
            "##..##..##..##..##..##..##..##..##..##..
###...###...###...###...###...###...###.
####....####....####....####....####....
#####.....#####.....#####.....#####.....
######......######......######......####
#######.......#######.......#######.....
",
        ];

        assert_eq!(_day_10_pt2(test_input), expected);
    }

    #[test]
    fn day_10() {
        let test_input = fs::read_to_string("data/day_10/input").unwrap();
        assert_eq!(_day_10_pt1(&test_input), 14760);
        let expected = get_day_10_pt2_expected();
        let result = _day_10_pt2(&test_input);
        print!("{}", result.get(0).unwrap());
        assert_eq!(result, expected);
    }

    #[test]
    fn test_day_11() {
        let test_input = "Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1";

        assert_eq!(_day_11_pt1(test_input), 10605);
    }

    #[test]
    fn day_11() {
        let test_input = fs::read_to_string("data/day_11/input").unwrap();
        assert_eq!(_day_11_pt1(&test_input), 6391);
    }

    fn get_test_input_day_10() -> &'static str {
        "addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop"
    }

    fn get_day_10_pt2_expected() -> Vec<String> {
        vec!["####.####..##..####.###..#..#.###..####.\n#....#....#..#.#....#..#.#..#.#..#.#....\n###..###..#....###..#..#.#..#.#..#.###..\n#....#....#.##.#....###..#..#.###..#....\n#....#....#..#.#....#.#..#..#.#.#..#....\n####.#.....###.####.#..#..##..#..#.####.\n".to_string()]
    }
}
